$(() => {
    $('[data-map]').each(function() {
        let map = new Map($(this));
    });

    let sliders = document.getElementsByClassName("longread-section-slider__images");

    if (sliders.length) {
        new Swiper(sliders, {
            speed: 400,
            navigation: {
                nextEl: ".longread-section-slider-next",
                prevEl: ".longread-section-slider-prev",
            },
            pagination: {
                el: ".longread-section-slider-pagination",
                type: "fraction",
            },
            resistance: true,
            resistanceRatio: 0,
            loop: true
        });

        // console.log(swiperSlider.slides.length, swiperSlider)

        // if (swiperSlider.slides.length <= 1) {
        //     swiperSlider.$el.addClass('_single')
        // }
    }

    let sliderLongread6 = new Swiper('.slider-longread-6', {
        speed: 400,
        pagination: {
            el: '.slider-longread-6__pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.slider-longread-6__next',
            prevEl: '.slider-longread-6__prev',
        },
        loop:true
    });

    let sliderDecor = new Swiper('.slider-decor', {
        speed: 400,
        pagination: {
            el: '.slider-decor__pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.slider-decor__next',
            prevEl: '.slider-decor__prev',
        },
        loop:true
    });

    let sliderPlans = new Swiper('.longread-slider-plans', {
        speed: 400,
        slidesPerView: 'auto',
        resistanceRatio: 0,
        scrollbar: {
            el: '.swiper-scrollbar',
            hide: false
        },
        breakpoints: {
            575: {
                slidesPerView: 1,
                scrollbar: {
                    hide: false
                }
            }
        },
        observer: true,
        observeParents: true
    });

    let fullSlider = new Swiper('.longread-section-slider-full', {
        speed: 600,
        navigation: {
            nextEl: ".longread-section-slider-full__next",
            prevEl: ".longread-section-slider-full__prev",
        },
        pagination: {
            el: ".longread-section-slider-pagination",
            type: "fraction",
        },
        resistance: true,
        resistanceRatio: 0,
        loop: true
    } )
    console.log(fullSlider)
    // fullSlider.loop(false);

    console.log(fullSlider.slides.length - 2)

    if (fullSlider.slides.length - 2 <= 1) {
        fullSlider.$el.addClass('_single')
        }

    $('.longread-mapInfra-btns__infra').click(function () {
        $('.longread4-category-links').removeClass('longread4-category-links_hidden');
        $('.longread4-category-links').addClass('longread4-category-links_visible');
    });

    $('.longread-mapInfra-btns__map').click(function () {
        $('.longread4-category-links').removeClass('longread4-category-links_visible');
        $('.longread4-category-links').addClass('longread4-category-links_hidden');
    });
});
