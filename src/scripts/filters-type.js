const FLATS_PER_PAGE = 24;

let cardTplStr = `
/*=require ./includes/chunks/FlatType.tpl */
`;

let flatsListBuilder = new FlatsList({
    tpl: cardTplStr,
    $mountEl: $("#FlatTypeResult"),
});


let $progress = $(".PickupFlatLayout_progress");
let $resultsCont = $(".PickupFlatLayout_resultsCont");

let filter = new FilterForm("#filterForm", {
    submitHandler: ($filterForm) => {
        // console.log(" --- submitHandler fired---", "filter.offset: ", filter.offset);

        $resultsCont.addClass("PickupFlatLayout_resultsCont-loading");
        $progress.show().animate({ width: "33%", opacity: 1 }, 600);

        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                action: "get_flats",
                limit: FLATS_PER_PAGE,
                offset: filter.offset,
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                        if (percent === 100) {
                            $progress.animate({ opacity: 0 }, 350, () => {
                                $progress.removeAttr("style");
                            });
                        }
                    }

                    else {
                        $progress.stop(true, false).animate({ width: '100' + "%" }, 400);
                        $progress.animate({ opacity: 0 }, 350, () => {
                            $progress.removeAttr("style");
                        });
                        
                    }
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                // fix pics urls in development enviroment
                if (window.location.href.indexOf("/filters.html") != -1) {
                    console.warn("rewrite json response for dev enviroment usage...");
                    jsonResponse.flats.forEach((flt) => {
                        flt.pic = "https://etalonsad.ru/" + flt.pic;
                        flt.href = "/flat.html";
                    });
                    console.log('jsonResponse', jsonResponse)
                    
                    $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");
                }

                {
                    let promos = jsonResponse.promos;
                    if (promos && promos.length) {
                        promos.forEach((promo)=>{
                            jsonResponse.flats.splice(promo.order-1, 0, promo);
                        });
                    }
                }

                // render plans
                $("#filteredFlatsCounter").html(jsonResponse.total);

                flatsListBuilder.render({
                    data: jsonResponse,
                });

                
                $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});


$("#FlatTypeResult").tooltip({
    items: ".actionIcon",
    classes: {
        "ui-tooltip": "actionIconTooltip",
    },
});
