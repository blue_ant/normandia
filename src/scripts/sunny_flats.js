
var actNum = window.location.hash.slice(-1) - 1;
if ( actNum < 0 ) {
    actNum = 0;
}
$("#UniqPlansTabs").tabs({
    //active: 6,
    active: actNum,
    activate: function(event, ui) {
        var scrollTop = $(window).scrollTop();
        //window.location.hash = ui.newPanel.attr('id');
        window.location.hash = ui.newPanel.attr('data-hash');
        $(window).scrollTop(scrollTop);
    }
});

var detectSlider = function () {
  //"use strict";

  // breakpoint where swiper will be destroyed
  // and switches to a dual-column layout
  const breakpoint = window.matchMedia("(min-width:767.98px)");
  console.log('sliderdete')
  // keep track of swiper instances to destroy later
  let mySwiper;

  const breakpointChecker = function() {
      // if larger viewport and multi-row layout needed
      if (breakpoint.matches === true) {
          // clean up old instances and inline styles when available
          if (mySwiper !== undefined) {
            mySwiper.forEach(function (e, i) {
              console.log(e)
              e.destroy(true, true);
            })
          }

          // or/and do nothing
          return;

          // else if a small viewport and single column layout needed
      } else if (breakpoint.matches === false) {
          // fire small viewport version of swiper
          return enableSwiper();
      }
  };

  const enableSwiper = function() {
      mySwiper = new Swiper('.SunnyFlats_angles',{
        slidesPerView: 1,
        direction: 'horizontal',
        observer: true,
        observeParents: true,
        loop: false,
        autoHeight: false,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
      })
  };

  // keep an eye on viewport size changes
  breakpoint.addListener(breakpointChecker);

  // kickstart
  breakpointChecker();
}

$(window).on('resize load', function () {
  detectSlider(); /* IIFE end */
})