let furnishComponentNav = [];


$('.furnish-page-components__item').each(function (index, el) {
    let title = $(this).data('furnish-title');
    furnishComponentNav.push(title);
});


let furnishComponents = new Swiper('.furnish-page-components', {
    speed: 400,
    resistance: true,
    resistanceRatio: 0,
    simulateTouch:false,
    touchEventsTarget: 'wrapper',
    pagination: {
        el: '.furnish-page-components__pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + ' ' + 'furnish-page-components__link' + '" data-id="' + index + '">' + (furnishComponentNav[index]) + '</span>';
        },
    },
    on: {
        slideChange: function () {
            let idActiveMenu =  $('.swiper-pagination-bullet-active').data('id');
            $('.furnish-components-mobile').removeClass('furnish-components-mobile_active')
            $('[data-id-menu="'+idActiveMenu+'"]').addClass('furnish-components-mobile_active');
        },
    }
});


$('.furnish-labels__item').each(function(index, el) {
    let $el = $(el);
    let contentTooltip = $el.find('.furnish-labels__content').html();

    $el.tooltip({
        items: $el,
        classes: {
            "ui-tooltip": "furnish-labels__tooltip"
        },
        track: false,
        tooltipClass: "tooltip-label",
        content:contentTooltip,
        position: { my: "left+15 top+5", at: "left bottom", collision: "flipfit" }
    })
    // $el.tooltip('open')
});

$( ".furnish-components-mobile" ).accordion({
    active:0,
    collapsible:true,
    classes: {
        "ui-accordion": "furnish-components-mobile"
    },
    icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus" },
    heightStyle: 'content',
    header: '.furnish-components-mobile__title',
    beforeActivate: function(event, ui) {
        // The accordion believes a panel is being opened
        if (ui.newHeader[0]) {
            var currHeader  = ui.newHeader;
            var currContent = currHeader.next('.ui-accordion-content');
            // The accordion believes a panel is being closed
        } else {
            var currHeader  = ui.oldHeader;
            var currContent = currHeader.next('.ui-accordion-content');
        }
        // Since we've changed the default behavior, this detects the actual status
        var isPanelSelected = currHeader.attr('aria-selected') == 'true';

        // Toggle the panel's header
        currHeader.toggleClass('ui-corner-all',isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top',!isPanelSelected).attr('aria-selected',((!isPanelSelected).toString()));

        // Toggle the panel's icon
        currHeader.children('.ui-icon').toggleClass('ui-icon-pl',isPanelSelected).toggleClass('ui-icon-mn',!isPanelSelected);

        // Toggle the panel's content
        currContent.toggleClass('accordion-content-active',!isPanelSelected)
        if (isPanelSelected) { currContent.slideUp(); }  else { currContent.slideDown(); }

        return false; // Cancels the default action
    }
});

$('.furnish-page-components__link').click(function () {
    let id = $(this).data('id');
    $('.furnish-components-mobile').removeClass('furnish-components-mobile_active')
    $('[data-id-menu="'+id+'"]').addClass('furnish-components-mobile_active');
});




