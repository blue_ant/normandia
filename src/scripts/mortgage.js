var $banks = $('[data-banks-body]');
var $flats = $('.calculator-flats');
var availableBanks = [];
var creditSumm     = 0;

const $modal  = $('.mortgage-modal');


$(function() {
    $("[data-mortgage-form]").on('submit', function(e) {
        e.preventDefault();

        let $form = $(this);

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit : 1,
            url    : window.location.href,
        });

        $.ajax({
            url  : $form.data("action"),
            type : $form.attr("method"),
            data : dataToSend,
        }).done((response) => {
            let errorCode = parseInt(response.code);

            if (errorCode === 0) {
                let successText = `
                    <div class="page-sidebar__inner call-form-inner_sendMessage">
                        <div class="call-form-sidebar__tlt call-form-sidebar__tlt_ok">Заявка отправлена!</div>
                        <div class="call-form-sidebar__text">
                            ${response.success}
                        </div>
                        <button class="call-form-sidebar__btn call-form-sidebar__btn_ok">ok</button>
                    </div>`;

                $form.trigger('reset').parent().hide();

                window.requestAnimationFrame(() => {
                    $form.parent().hide().after(successText);
                });

            } else {
                alert("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
            }
        }).always(( /*response*/ ) => {

        });

        return false;
    });

    $('[data-slider]').each(function() {
        let slider = new RangeSliderMortgage($(this));
    });

});

$(document).ready(() => {
    let morterSorter = new SorterMortgage();
});



