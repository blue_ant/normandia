var burger = $(".header__burger");

let homeBanner = new Swiper(".home-banner-list", {
    effect: "slide",
    slidesPerView: 1,
    resistanceRatio: 0,
    autoplay: {
        delay: 4200,
        disableOnInteraction: false,
    },
    speed: 1000,
    scrollbar: {
        el: ".home-banner-list .swiper-scrollbar",
        draggable: true,
        snapOnRelease: true,
    },
});

$(".video-about-btn").fancybox({
    /*beforeClose: function(instance, slide) {
        burger.show("fast");
    },*/
    smallBtn: true,
    hideScrollbar: false,
    backFocus: false,
    modal: false,
    buttons: [],
    touch: false,
    baseClass: "video-about-modal",
    btnTpl: {
        smallBtn:
        '<button data-fancybox-close class="close-gallery close-gallery_video" title="{{CLOSE}}">' +
        '<img src="img/close-popup-dark.svg" alt=""></img>' +
        "</button>",},

});

let hideScrollForBannerSlide = () => {
    let activeSlide = $('.home-banner-list').find('.swiper-slide-active');
    let scrollingIcon = $('.home-banner__scrolling');
    let scrollbaer = $('.home-banner .swiper-scrollbar');
    let slideIsBanner = activeSlide.hasClass('banner-slide');
    let slideDate = $('.home-banner__date');

    if (slideIsBanner) {
        scrollingIcon.addClass('home-banner__scrolling_hidden');
        scrollbaer.addClass('swiper-scrollbar_hidden');
        slideDate.addClass('home-banner__date_hidden');
    } else {
        scrollingIcon.removeClass('home-banner__scrolling_hidden');
        scrollbaer.removeClass('swiper-scrollbar_hidden');
        slideDate.removeClass('home-banner__date_hidden');
    }
}

hideScrollForBannerSlide();

homeBanner.on('slideChangeTransitionEnd', function () {
    console.log('slide change')
    hideScrollForBannerSlide();
});

