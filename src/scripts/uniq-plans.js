$( function() {
    var actNum = window.location.hash.slice(-1) - 1;
    if ( actNum < 0 ) {
        actNum = 0;
    }
    $( "#UniqPlansTabs" ).tabs({
        //active: 6,
        active: actNum,
        activate: function(event, ui) {
            var scrollTop = $(window).scrollTop();
            //window.location.hash = ui.newPanel.attr('id');
            window.location.hash = ui.newPanel.attr('data-hash');
            $(window).scrollTop(scrollTop);
        }
    });
});

$(function() {
    //"use strict";

    // breakpoint where swiper will be destroyed
    // and switches to a dual-column layout
    const breakpoint = window.matchMedia("(min-width:1279.98px)");

    // keep track of swiper instances to destroy later
    let mySwiper;

    const breakpointChecker = function() {
        // if larger viewport and multi-row layout needed
        if (breakpoint.matches === true) {
            // clean up old instances and inline styles when available
            if (mySwiper !== undefined) mySwiper.destroy(true, true);

            // or/and do nothing
            return;

            // else if a small viewport and single column layout needed
        } else if (breakpoint.matches === false) {
            // fire small viewport version of swiper
            return enableSwiper();
        }
    };

    const enableSwiper = function() {
        mySwiper = new Swiper(".swiper-container", {
            loop: false,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            slidesPerView: 'auto',
            breakpoints: {
                480: {
                    //initialSlide: 1,
                    centeredSlides: true,
                },
            }
        });
    };

    // keep an eye on viewport size changes
    breakpoint.addListener(breakpointChecker);

    // kickstart
    breakpointChecker();
})(); /* IIFE end */