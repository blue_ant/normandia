const furnishBanner = new Swiper(".furnish_banner_list", {
    effect: "slide",
    slidesPerView: 1,
    speed: 1000,
    loop: true,
    pagination: {
        el: ".furnish_banner_pagination",
        type: "fraction",
    },
    navigation: {
        nextEl: ".furnish_banner_arrow_right",
        prevEl: ".furnish_banner_arrow_left",
    },
});

window.addEventListener("resize", () => {
    furnishBanner.update();
});

const furnishBlock = document.querySelector(".furnish");
const moreButton = document.querySelector(".btn-arrow");
const closeButton = furnishBlock.querySelector(".furnish_description_close");
const furnishDescText = furnishBlock.querySelector(".furnish_description_more-text");

new PerfectScrollbar(furnishDescText, {
    swipeEasing: true,
    suppressScrollX: true,
    wheelPropagation: true
});

moreButton.addEventListener("click", (e) => {
    e.preventDefault();
    furnishBlock.classList.add("furnish-open");
});

closeButton.addEventListener("click", (e) => {
    e.preventDefault();
    furnishBlock.classList.remove("furnish-open");
});
