new Fitblock(document.querySelector(".Fitblock"));

let styleMaskCont = document.getElementById('styleMask');

$('.Genplan_poligonLink').hover(
    function () {
        let hoverClassName = this.classList.item(1);
        let styles = "." + hoverClassName + "{opacity:1; visibility:visible;}";
        styleMaskCont.innerHTML = styles;
    },
    function () {
        styleMaskCont.innerHTML = '';
    }
)

$('.Genplan_marker').hover(
    function () {
        let hoverClassName = this.classList.item(1);
        let styles = "." + hoverClassName + "{opacity:1; visibility:visible;}";
        styleMaskCont.innerHTML = styles;
        let marker = $('.Genplan_poligonLink.' + hoverClassName);
        marker.trigger('mouseenter');
    },
    function () {
        let hoverClassName = this.classList.item(1);
        let marker = $('.Genplan_poligonLink.' + hoverClassName);
        styleMaskCont.innerHTML = '';
        marker.trigger('mouseleave');
    }
)


$(".SvgWrap").tooltip({
    items: ".Genplan_poligonLink",
    classes: {
        "ui-tooltip": "Genplan_countFlats",
    },
    track: true
});

let setLineForLabelsBld = ()=> {
    let $label = $('.Genplan_label[data-bld]');

    $label.each(function (index, el) {
        let labelsPosWithHeight = $(this).position().top + $(this).outerHeight();
        let numBld = $(this).data('bld');

        let marker = $('.Genplan_marker[data-bld="'+numBld+'"]');

        let markerPosWithHeight = marker.position().top + marker.outerHeight();

        let heightLine = markerPosWithHeight - labelsPosWithHeight;

        if (numBld === 'bld5') {
            $(this).find('.Genplan_label_line').css("height", `${heightLine + 20}px`);
            $(this).find('.Genplan_label_line').addClass('_bld5')
        } else {
            $(this).find('.Genplan_label_line').css("height", `${heightLine}px`);
        }

    })
}


setLineForLabelsBld();

$(window).on('resize', function () {
    setLineForLabelsBld();
})
