const FLATS_PER_PAGE = 24;

setTimeout(function() {
    tinysort($(".Tbl_tr-commercialFlat"), { selector: ".Tbl_td-price", order: "asc", natural: true });
}, 100);

$(".Sorter_lbl").on("click", function() {
    $(this).toggleClass("Sorter_lbl-desc");
    let isDesc = $(this).hasClass("Sorter_lbl-desc");
    let val = $(this).data("val");

    if (isDesc) {
        tinysort($(".Tbl_tr-commercialFlat"), { selector: ".Tbl_td-" + val + "", order: "desc", natural: true });
    } else {
        tinysort($(".Tbl_tr-commercialFlat"), { selector: ".Tbl_td-" + val + "", order: "asc", natural: true });
    }
});

new ViewSwitch(".ViewSwitch", {
    /*onChange: ($activeBtn) => {
        console.log($activeBtn.attr('href'));
    }*/
});

let cardTplStr = `
/*=require ./includes/chunks/FlatCommercial.tpl */
`;

let flatsListBuilder = new FlatsList({
    tpl: cardTplStr,
    $mountEl: $("#FlatCommercialList"),
});

$.ajax({
    url: "/ajax/flats-commercial.json",
    dataType: "json",
    method: "GET",
    async: true,
})
    .done((jsonResponse) => {
        flatsListBuilder.render({
            data: jsonResponse,
        });
    })
    .fail(() => {
        alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
    });
