//alert("скрипт для раздела парковка");
let parkingSorter = new Sorter(".parking-sorter");

const PARKING_PER_PAGE = 14;

let parkingTplStr = `
/*=require ./includes/chunks/Parking.tpl */
`;

let parkingListBuilder = new FlatsList({
    tpl: parkingTplStr,
    $mountEl: $("#parkingResult"),
});

let $pager = $(".parking-pagination").pagination({
    itemsOnPage: PARKING_PER_PAGE,
    prevText: "&nbsp;",
    nextText: "&nbsp;",
    displayedPages: 3,
    ellipsePageSet: false,
    edges: 0,
    onPageClick: (num) => {
        filter.offset = (num - 1) * PARKING_PER_PAGE;
        console.log("pager click!!!");
        filter.$filterForm.trigger("submit");
        $("html,body").animate({ scrollTop: parkingSorter.$el.parent().offset().top }, 700, "easeInOutExpo");
        return false;
    },
});

let parkingPreloader = $('.parking-preloader');

let filter = new ParkingFilter("#parkingFilter", {
    submitHandler: ($filterForm) => {

        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                action: "get_flats",
                limit: PARKING_PER_PAGE,
                offset: filter.offset,
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        if (percent === 100) {
                            parkingPreloader.addClass('parking-preloader_disabled');
                        }
                    } else {
                        parkingPreloader.addClass('parking-preloader_disabled');
                    }
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                parkingListBuilder.render({
                    data: jsonResponse,
                });


                $pager.pagination("updateItems", jsonResponse.total);

                {
                    let currentPage = filter.offset / PARKING_PER_PAGE + 1;
                    $pager.pagination("drawPage", currentPage);
                }
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});
