class ViewSwitch {

    constructor(el, opts = {}) {

        let $el = $(el);
        let $btns = $el.find('a');

        let $styleTagInHead = $('<style id="view_switcher_generated_css"></style>');
        $styleTagInHead.appendTo(document.head);

        let possibleLastActiveEl = window.localStorage.getItem("view_switcher_last_active_view");

        if (possibleLastActiveEl) {
            $btns
                .removeClass('ViewSwitch_btn-active')
                .filter(`[href="${possibleLastActiveEl}"]`)
                .addClass('ViewSwitch_btn-active');
        }

        $btns.on('click', (event) => {
            event.preventDefault();
            $btns.removeClass('ViewSwitch_btn-active');

            let $activeBtn = $(event.currentTarget);
            $activeBtn.addClass('ViewSwitch_btn-active');

            window.localStorage.setItem("view_switcher_last_active_view", $activeBtn.attr('href'));

            this.updateCSS();
            if (opts.onChange) {
                opts.onChange($activeBtn);
            }

        });

        this.$el = $el;
        this.$btns = $btns;
        this.$styleTagInHead = $styleTagInHead;
        this.updateCSS();
    }

    updateCSS(){
        let selectorsToHide = $.map(this.$btns.not(".ViewSwitch_btn-active"), (el)=>{
            return el.getAttribute("href");
        });
        let generatedCSS = `${selectorsToHide.join(",")}{display:none!important}`;
        this.$styleTagInHead.html(generatedCSS);
    }
}