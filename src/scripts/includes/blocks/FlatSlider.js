
class FlatSlider {
	constructor(el, opts = {}) {
    let $el = $(el);
    let $thumbSlider = $el.find('[data-flat-slider]');
    let $thumbSlide = $thumbSlider.find('[data-flat-thumb-num]');
    console.log('flat init', $thumbSlider)
    let ThumbSwiper = new Swiper($thumbSlider, {
      loop: false,
      speed: 700,
      centeredSlides: true,
      spaceBetween: 16,
      slidesPerView: 4,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        768: {
          loop: false,
          speed: 700,
          centeredSlides: true,
          spaceBetween: 16,
          slidesPerView: 4,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
        }
      }
    });

    $thumbSlide.on('click', function (e) {
      e.preventDefault();
      let dataNum = $(this).data('flat-thumb-num');
      ThumbSwiper.slideTo(dataNum - 1);
    });

    ThumbSwiper.on('transitionEnd', function (e) {
      console.log('slide changed', e);
      $('[data-flat-slider-num]').hide();
      $(`[data-flat-slider-num="${ThumbSwiper.activeIndex + 1}"]`).fadeIn(700);
    });
	}

}
