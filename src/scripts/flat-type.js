let FavoriteIcon = () => {
    let icon = $('.FavoriteIcon');
    icon.click(function (e) {
        e.preventDefault();
        $(this).toggleClass('FavoriteIcon-active');
    })
};

FavoriteIcon();

$( ".Flat-type-plans" ).tabs({
    classes: {
        "ui-tabs-nav": "Flat-type-plans__nav",
    }
});
setTimeout(function() {
    tinysort($(".Tbl_tr-flatType"), { selector: ".Tbl_td-price", order: "asc", natural: true });

    $(".Sorter_lbl").on("click", function() {
        $(this).toggleClass("Sorter_lbl-desc");
        let isDesc = $(this).hasClass("Sorter_lbl-desc");
        let val = $(this).data("val");

        if (isDesc) {
            tinysort($(".Tbl_tr-flatType"), { selector: ".Tbl_td-" + val + "", order: "desc", natural: true });
        } else {
            tinysort($(".Tbl_tr-flatType"), { selector: ".Tbl_td-" + val + "", order: "asc", natural: true });
        }
    });

}, 100);


// $('.Tbl_tr .FavoriteIcon').click(function(e){
//     e.stopPropagation();
//     e.preventDefault();
//     $(this).toggleClass('FavoriteIcon-active');
//     // let favIcon = 'FavoriteIcon';
//     // if (e.target.dataset.name === favIcon) {
//     //     e.preventDefault();
//     //     FavoriteIcon();
//     // }
// });

$("#FlatTypeResult").on('click', ".FavoriteIcon", (event)=> {
    event.preventDefault();
    event.stopPropagation();
    FavoriteIcon();
});
