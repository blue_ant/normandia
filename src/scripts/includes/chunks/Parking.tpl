<% if(data.total !== 0) {%>

<div class="Tbl Tbl-parking" id="listViewResult">
    <% _.forEach(data.parkingItems, function(item) { %>

    <div class="Tbl_tr Tbl_tr-parking">
        <div class="Tbl_td Tbl_td-parking"><%= item.house %></div>
        <div class="Tbl_td Tbl_td-parking"><%= item.num %></div>
        <div class="Tbl_td Tbl_td-parking"><%= item.floor %> этаж</div>
        <div class="Tbl_td Tbl_td-parking"><%= item.price.toLocaleString("ru-RU") %> ₽</div>
        <div class="Tbl_td Tbl_td-parking">
            <a class="parking-plan <%= item.plan === '' ? 'parking-plan_disabled' : '' %>" href="<%= item.plan %>" target="_blank"><span>Загрузить</span></a>
        </div>
    </div>

    <% }); %>

</div>

    <div class="parking-mobile">
        <% _.forEach(data.parkingItems, function(item) { %>
        <div class="parking-mobile__item">
            <div class="parking-mobile__info">
                <div class="parking-mobile__row">
                    <div class="parking-mobile__param"><%= item.house %> корпус</div>
                    <div class="parking-mobile__param"><%= item.num %></div>
                    <div class="parking-mobile__param"><%= item.floor %> этаж</div>
                </div>
                <div class="parking-mobile__price">
                    Цена: <span><%= item.price.toLocaleString("ru-RU") %> ₽</span>
                </div>
            </div>
            <div class="parking-mobile__plan">
                <a class="parking-plan <%= item.plan === '' ? 'parking-plan_disabled' : '' %>" href="<%= item.plan %>" target="_blank"><span>Загрузить</span></a>
            </div>
        </div>
        <% }); %>
    </div>



<%}else{%>
<div class="NoPlansFound" style="text-align:center;padding:40px;"><p class="NoPlansFound_text">Квартиры по заданным параметрам не найдены,<br class="hidden-xs"> пожалуйста измените параметры поиска.</p></div>
<% }; %>